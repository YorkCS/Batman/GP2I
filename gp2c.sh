#!/bin/bash

rm -f $1/gp2.log
[ -d $2 ] || mkdir $2

if [ -z "${GP2_FLAGS}" ]; then
  gp2 -l /gp2/lib -o $2 $1/$3 &> $1/gp2.log
else
  gp2 $GP2_FLAGS -l /gp2/lib -o $2 $1/$3 &> $1/gp2.log
fi

if grep -q "Usage:" $1/gp2.log; then
  >&2 echo 'Invalid arguments provided to the GP2 compiler. Build aborted.'
  exit 1
fi

if grep -q "Error: " $1/gp2.log || grep -q "Error at " $1/gp2.log || grep -q "Build aborted." $1/gp2.log || grep -q "Failed to compile program!" $1/gp2.log; then
  >&2 sed '/^$/d' $1/gp2.log
  exit 1
fi

cd $2
./build.sh >> /dev/null || exit 1
