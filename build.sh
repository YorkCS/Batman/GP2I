#!/bin/bash

export CFLAGS="-O3 -Wall -Wno-stringop-overflow"

aclocal
autoconf
autoreconf --install
automake --add-missing --foreign
./configure
make -j4
